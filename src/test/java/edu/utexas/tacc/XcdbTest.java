/**
 * 
 */
package edu.utexas.tacc;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @author mrhanlon
 *
 */
@ContextConfiguration("/xcdb-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class XcdbTest {

	@Autowired
	@Qualifier("dataSource")
	private DataSource dataSource;
	
	@Test
	public void testConnection() throws SQLException {
		Connection con = dataSource.getConnection();
		PreparedStatement ps = con.prepareStatement("select 1");
		ps.execute();
		ps.close();
		con.close();
	}
}
