/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.Permission;

/**
 * @author mrhanlon
 *
 */
public class PermissionTest extends BaseXrasTest {

	@Autowired
	private PermissionService permissionService;
	
	@Test
	public void permissionList() throws ServiceException {
		List<Permission> permissions = permissionService.list(session);
		for (Permission p : permissions) {
			getLogger().debug(p.getDisplayPermissionType());
		}
		
		session = new XrasSession("hackwort");
		permissions = permissionService.list(session);
		for (Permission p : permissions) {
			getLogger().debug(p.getDisplayPermissionType());
		}
	}

}
