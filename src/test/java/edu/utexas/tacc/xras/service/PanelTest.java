/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.Panel;
import edu.utexas.tacc.xras.service.PanelService;

/**
 * @author mrhanlon
 *
 */
public class PanelTest extends BaseXrasTest {
	
	@Autowired
	private PanelService panelService;

	@Test
	public void read() throws ServiceException, JsonProcessingException {
		Panel read = panelService.read(session, new Panel(1));
		Assert.assertEquals(1, read.getId());
	}
	
	@Test
	public void list() throws ServiceException {
		List<Panel> models = panelService.list(session);
		for (Panel p : models) {
			getLogger().debug(p.getPanelName());
		}
		Assert.assertTrue(models.size() > 0);
	}

}
