/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.type.ActionType;
import edu.utexas.tacc.xras.model.type.AllocationDateType;
import edu.utexas.tacc.xras.model.type.AttributeSetRelationType;
import edu.utexas.tacc.xras.model.type.AttributeSetType;
import edu.utexas.tacc.xras.model.type.DocumentType;
import edu.utexas.tacc.xras.model.type.FieldOfScienceType;
import edu.utexas.tacc.xras.model.type.OpportunityStateType;
import edu.utexas.tacc.xras.model.type.PermissionType;
import edu.utexas.tacc.xras.model.type.RequestStatusType;
import edu.utexas.tacc.xras.model.type.RequestType;
import edu.utexas.tacc.xras.model.type.ResourceNumberType;
import edu.utexas.tacc.xras.model.type.ResourceType;
import edu.utexas.tacc.xras.model.type.RoleType;
import edu.utexas.tacc.xras.model.type.UnitType;
import edu.utexas.tacc.xras.service.type.ActionTypeService;
import edu.utexas.tacc.xras.service.type.AllocationDateTypeService;
import edu.utexas.tacc.xras.service.type.AttributeSetRelationTypeService;
import edu.utexas.tacc.xras.service.type.AttributeSetTypeService;
import edu.utexas.tacc.xras.service.type.DocumentTypeService;
import edu.utexas.tacc.xras.service.type.FieldOfScienceTypeService;
import edu.utexas.tacc.xras.service.type.OpportunityStateTypeService;
import edu.utexas.tacc.xras.service.type.PermissionTypeService;
import edu.utexas.tacc.xras.service.type.RequestStatusTypeService;
import edu.utexas.tacc.xras.service.type.RequestTypeService;
import edu.utexas.tacc.xras.service.type.ResourceNumberTypeService;
import edu.utexas.tacc.xras.service.type.ResourceTypeService;
import edu.utexas.tacc.xras.service.type.RoleTypeService;
import edu.utexas.tacc.xras.service.type.UnitTypeService;

/**
 * @author mrhanlon
 *
 */
public class TypeTest extends BaseXrasTest {

	@Autowired
	private ActionTypeService actionTypeService;

	@Autowired
	private AllocationDateTypeService allocationDateTypeService;

  @Autowired
  private AttributeSetTypeService attributeSetTypeService;

  @Autowired
  private AttributeSetRelationTypeService attributeSetRelationTypeService;

	@Autowired
	private DocumentTypeService documentTypeService;

	@Autowired
	private FieldOfScienceTypeService fieldOfScienceTypeService;

	@Autowired
	private OpportunityStateTypeService opportunityStateTypeService;

	@Autowired
	private PermissionTypeService permissionTypeService;

	@Autowired
	private RequestStatusTypeService requestStatusTypeService;

	@Autowired
	private RequestTypeService requestTypeService;

	@Autowired
	private ResourceNumberTypeService resourceNumberTypeService;

	@Autowired
	private ResourceTypeService resourceTypeService;

	@Autowired
	private RoleTypeService roleTypeService;

	@Autowired
	private UnitTypeService unitTypeService;

	@Test
	public void listActionTypes() throws ServiceException, JsonProcessingException {
		List<ActionType> models = actionTypeService.list(session);
		for (ActionType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listAllocationDateTypes() throws ServiceException, JsonProcessingException {
		List<AllocationDateType> models = allocationDateTypeService.list(session);
		for (AllocationDateType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

  @Test
  public void listAttributeSetTypes() throws ServiceException, JsonProcessingException {
    List<AttributeSetType> models = attributeSetTypeService.list(session);
    for (AttributeSetType model : models) {
      getLogger().debug(model.toJSON().toString());
    }
    Assert.assertTrue(models.size() > 0);
  }

  @Test
  public void listAttributeSetRelationTypes() throws ServiceException, JsonProcessingException {
    List<AttributeSetRelationType> models = attributeSetRelationTypeService.list(session);
    for (AttributeSetRelationType model : models) {
      getLogger().debug(model.toJSON().toString());
    }
    Assert.assertTrue(models.size() > 0);
  }

	@Test
	public void listDocumentTypes() throws ServiceException, JsonProcessingException {
		List<DocumentType> models = documentTypeService.list(session);
		for (DocumentType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listFieldOfScienceTypes() throws ServiceException, JsonProcessingException {
		List<FieldOfScienceType> models = fieldOfScienceTypeService.list(session);
		for (FieldOfScienceType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listOpportunityStateTypes() throws ServiceException, JsonProcessingException {
		List<OpportunityStateType> models = opportunityStateTypeService.list(session);
		for (OpportunityStateType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listPermissionTypes() throws ServiceException, JsonProcessingException {
		List<PermissionType> models = permissionTypeService.list(session);
		for (PermissionType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listRequestStatusTypes() throws ServiceException, JsonProcessingException {
		List<RequestStatusType> models = requestStatusTypeService.list(session);
		for (RequestStatusType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listRequestTypes() throws ServiceException, JsonProcessingException {
		List<RequestType> models = requestTypeService.list(session);
		for (RequestType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listResourceNumberTypes() throws ServiceException, JsonProcessingException {
		List<ResourceNumberType> models = resourceNumberTypeService.list(session);
		for (ResourceNumberType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listResourceTypes() throws ServiceException, JsonProcessingException {
		List<ResourceType> models = resourceTypeService.list(session);
		for (ResourceType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listRoleTypes() throws ServiceException, JsonProcessingException {
		List<RoleType> models = roleTypeService.list(session);
		for (RoleType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

	@Test
	public void listUnitTypes() throws ServiceException, JsonProcessingException {
		List<UnitType> models = unitTypeService.list(session);
		for (UnitType model : models) {
			getLogger().debug(model.toJSON().toString());
		}
		Assert.assertTrue(models.size() > 0);
	}

}
