/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.util.List;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.model.Resource;

/**
 * @author mrhanlon
 *
 */
public class ResourceTest extends BaseXrasTest {

	@Autowired
	private ResourceService resourceService;

	@Test
	public void read() throws ServiceException  {
		Resource model = new Resource(530154);
		Resource read = resourceService.read(session, model);
		getLogger().info(read.toJSON());
		Assert.assertEquals(model.getId(), read.getId());
	}

	@Test
	public void list() throws ServiceException {
		List<Resource> models = resourceService.list(session);
		for (Resource model : models) {
			getLogger().debug(model.getId() + ": " + model.getResourceName());
		}
	}

}
