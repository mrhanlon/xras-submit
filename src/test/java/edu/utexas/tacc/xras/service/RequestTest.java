/**
 *
 */
package edu.utexas.tacc.xras.service;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;
import org.junit.Ignore;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.google.common.base.Predicate;
import com.google.common.collect.Iterables;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.BinaryResponse;
import edu.utexas.tacc.xras.model.AttributeSet;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.model.OpportunityAttribute;
import edu.utexas.tacc.xras.model.OpportunityAttributeValue;
import edu.utexas.tacc.xras.model.OpportunityResource;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.model.Publication;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestAction;
import edu.utexas.tacc.xras.model.RequestAllocationDate;
import edu.utexas.tacc.xras.model.RequestDocument;
import edu.utexas.tacc.xras.model.RequestFos;
import edu.utexas.tacc.xras.model.RequestGrant;
import edu.utexas.tacc.xras.model.RequestPublication;
import edu.utexas.tacc.xras.model.RequestResource;
import edu.utexas.tacc.xras.model.RequestRole;
import edu.utexas.tacc.xras.model.Resource;
import edu.utexas.tacc.xras.model.ResourceAttribute;
import edu.utexas.tacc.xras.model.ResourceAttributeValue;
import edu.utexas.tacc.xras.model.type.ActionType;
import edu.utexas.tacc.xras.model.type.RequestType;
import edu.utexas.tacc.xras.service.type.ActionTypeService;
import edu.utexas.tacc.xras.service.type.FieldOfScienceTypeService;
import edu.utexas.tacc.xras.service.type.RequestTypeService;
import edu.utexas.tacc.xras.service.type.RoleTypeService;

/**
 * @author mrhanlon
 *
 */
public class RequestTest extends BaseXrasTest {
	
	@Autowired
	private RequestService requestService;
	
	@Autowired
	private PersonService personService;
	
	@Autowired
	private OpportunityService opportunityService;
	
	@Autowired
	private RequestDocumentService requestDocumentService;

	@Autowired
	private RequestTypeService requestTypeService;
	
	@Autowired
	private RoleTypeService roleTypeService;
	
	@Autowired
	private ActionTypeService actionTypeService;

	@Autowired
	private FieldOfScienceTypeService fosService;
	
	@Autowired
	private ResourceService resourceService;
	
	private static int REQUEST_TEST_ID = 0;

	@Test
	public void read() throws ServiceException {
		Request model = requestService.read(session, new Request(35));
		Assert.assertEquals(35, model.getId());
	}

	@Test
	public void list() throws ServiceException {
		List<Request> requests = requestService.list(session);
		for (Request r : requests) {
			getLogger().debug("[" + r.getId() + "] " + r.getTitle() + ": " + r.getAbstractText());
		}
		Assert.assertTrue(requests.size() > 0);
	}

	/**
	 * This test is ignored for now so a new request isn't created every time tests are run.
	 * TODO: test linking this test to a delete test?
	 * @throws ServiceException
	 */
	@Test
	public void createRequest() throws ServiceException {
		
		// get opportunity
		Opportunity opportunity = new Opportunity(1);
		opportunityService.read(session, opportunity);
		
		// get request type
		RequestType newRequestType = null;
		List<RequestType> types = requestTypeService.list(session);
		for (RequestType type : types) {
			if (type.getRequestType().equals("New")) {
				newRequestType = type;
				break;
			}
		}
		
		if (newRequestType == null) {
			Assert.fail("Unable to load RequestType:New");
		}

		// create request
		Request request = requestService.newRequest(session, new Request(), opportunity, newRequestType);
		
		Assert.assertTrue("Request was created, id assigned", request.getId() > 0);
		REQUEST_TEST_ID = request.getId();
	}
	
	@Test
	public void updateRequest() throws ServiceException {
		if (REQUEST_TEST_ID == 0) {
			getLogger().warn("REQUEST_TEST_ID was not set. This test is dependent on RequestTest::createRequest.");
			return;
		}
		Request request = requestService.read(session, new Request(REQUEST_TEST_ID));
		
		long time = System.currentTimeMillis();
		String title = "This is a test request - " + time;
		request.setTitle(title);
		request = requestService.addUpdateAttributes(session, request, "title");
		Assert.assertEquals(title, request.getTitle());
	}
	
	@Test
	public void deleteRequest() throws ServiceException {
		if (REQUEST_TEST_ID == 0) {
			getLogger().warn("REQUEST_TEST_ID was not set. This test is dependent on RequestTest::createRequest.");
			return;
		}
		
		Request request = requestService.delete(session, new Request(REQUEST_TEST_ID));
		Assert.assertTrue("Request is marked as deleted", request.isDeleted());
	}
	
	@Test
	public void addRole() throws ServiceException {
		Request request = requestService.read(session, new Request(35));
		
		Person person = personService.read(session, new Person("maytal"));
		
		RequestRole role = new RequestRole();
		role.setRole("PI");
		role.setBeginDate(new DateTime().withTimeAtStartOfDay().withZoneRetainFields(DateTimeZone.UTC));
		role.setEndDate(role.getBeginDate().plusYears(1));
		request = requestService.addRole(session, request, role, person);
		
		boolean hasRole = Iterables.contains(request.getRoles(), role);
		Assert.assertTrue("Role was successfully added", hasRole);
	}
	
	@Test @Ignore
	public void updateRole() throws ServiceException {
//		Request request = requestService.read(session, new Request(35));
//
//    final RequestRole roleType = new RequestRole();
//    roleType.setRole("PI");
//		RequestPersonRole piPersonRole = Iterables.find(request.getRoles(), new Predicate<RequestPersonRole>() {
//
//			@Override
//			public boolean apply(RequestPersonRole input) {
//				return Iterables.contains(input.getRoles(), roleType);
//			}
//		});
//		
//		Assert.assertNotNull(piPersonRole);
//		
//		RequestRole piRole = Iterables.find(piPersonRole.getRoles(), new Predicate<RequestRole>() {
//
//      @Override
//      public boolean apply(RequestRole input) {
//        return input.equals(roleType);
//      }
//		  
//		});
//		
//		DateTime end = piRole.getEndDate().plusYears(1);
//		piRole.setEndDate(end);
//		
//		request = requestService.updateRole(session, request, piRole);
//		piRole = Iterables.find(request.getRoles(), new Predicate<RequestRole>() {
//
//			@Override
//			public boolean apply(RequestRole input) {
//				return input.getRole().equals("PI");
//			}
//		});
//		
//		Assert.assertEquals(end, piPersonRole.getEndDate());
	}
	
	@Test @Ignore
	public void deleteRole() throws ServiceException {
//    Request request = requestService.read(session, new Request(35));
//		
//		Iterable<RequestPersonRole> pis = Iterables.filter(request.getRoles(), new Predicate<RequestPersonRole>() {
//			
//			@Override
//			public boolean apply(RequestPersonRole input) {
//			  RequestRole piRole = new RequestRole();
//			  piRole.setRole("PI");
//				return input.getRoles().contains(piRole);
//			}
//			
//		});
//		
//		for (Iterator<RequestPersonRole> it = pis.iterator(); it.hasNext(); ) {
//			RequestRole role = it.next().getRoles();
//			requestService.deleteRole(session, request, role);
//		}
//		
//		// refresh object from API
//		request = requestService.read(session, request);
//		
//		for (Iterator<RequestRole> it = pis.iterator(); it.hasNext(); ) {
//			RequestRole role = it.next();
//			Assert.assertFalse("Role is not in request roles", Iterables.contains(request.getRoles(), role));
//		}
	}
	
	@Test
	public void addAction() throws ServiceException {
		// mock action type from API
		ActionType actionType = new ActionType(1);
		actionType.setActionType("New");
		actionType.setDisplayActionType("New");
		
    Request request = requestService.read(session, new Request(35));
		
		int numberOfActions = request.getActions().size();
		
		RequestAction action = new RequestAction();
		action.setActionType(actionType.getActionType());
		request = requestService.addAction(session, request, action);
		
		Assert.assertEquals("Action added to request", numberOfActions + 1, request.getActions().size());
	}
	
	@Test @Ignore
	public void updateAction() {
		// TODO
		// this is for...changing the type of an action?
	}
	
	@Test
	public void deleteAction() throws ServiceException {
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has an action to delete", request.getActions().size() > 1);
		
		RequestAction action = Iterables.getLast(request.getActions());
		request = requestService.deleteAction(session, request, action);
		
		Assert.assertTrue("Action was marked as deleted", Iterables.getLast(request.getActions()).isDeleted());
	}
	
	@Test
	public void submitAction() throws ServiceException {
	  Request request = requestService.read(session, new Request(35));
	  
	  Assert.assertTrue("Request has an action to submit", request.getActions().size() > 1);
	  
	  RequestAction action = Iterables.getLast(request.getActions());
	  request = requestService.submitAction(session, request, action);
	  
	  action = Iterables.getLast(request.getActions());
	  getLogger().debug(action.toJSON());
	  Assert.assertEquals("ActionStatus is 'Submitted'", "Submitted", action.getActionStatus());
	}
	
	@Test
	public void addResource() throws ServiceException {
		
		// load request
    Request request = requestService.read(session, new Request(42));
		
		// request has an action to add a resource to
		RequestAction action = Iterables.getFirst(request.getActions(), null);
		Assert.assertNotNull("Request has an action", action);

		// create resource
		RequestResource resource = new RequestResource();
		resource.setId(530159); // Stampede
		resource.setAmount(new BigDecimal("50000.0"));
		resource.setComments("Lorem ipsum; " + DateTime.now());
		
		// save request resource
		request = requestService.addUpdateActionResource(session, request, action, resource);
		
		// confirm addition
		Assert.assertTrue("Resource added to action", request.getActions().get(0).getResources().contains(resource));
	}
	
	@Test
	public void updateResource() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));

		// request has an action
		RequestAction action = Iterables.getFirst(request.getActions(), null);
		Assert.assertNotNull("Request has an action", action);
		
		// and action has a resource
		RequestResource resource = Iterables.getFirst(action.getResources(), null);
		Assert.assertNotNull("RequestAction has a RequestResource to update");
		
		// update the requested amount
		BigDecimal newAmount = resource.getAmount().multiply(new BigDecimal(2));
		resource.setAmount(newAmount);
		request = requestService.addUpdateActionResource(session, request, action, resource);
		
		// confirm updated amount
		Assert.assertEquals("Updated amount", newAmount, request.getActions().get(0).getResources().get(0).getAmount());
	}
	
	@Test
	public void deleteResource() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));

		// get first action
		if (request.getActions().size() == 0) {
			Assert.fail("Request has no actions; RequestAction required to delete RequestResource");
		}
		RequestAction action = request.getActions().get(0);
		
		if (action.getResources().size() == 0) {
			Assert.fail("RequestAction has no resources; RequestResource required to test delete");
		}
		RequestResource resource = action.getResources().get(0);
		
		Request updated = requestService.deleteActionResource(session, request, action, resource);
		
		Assert.assertFalse("RequestResource in RequestAction", Iterables.contains(updated.getActions().get(0).getResources(), resource));
	}
	
	@Test
	public void addDocument() throws IOException, ServiceException {
		// create file for testing
		File upload = File.createTempFile("xras_", ".txt");
		FileOutputStream fos = new FileOutputStream(upload);
		fos.write("That's the shizzle that's the shizzle dolor sit amizzle, consectetuer adipiscing elizzle. Nullizzle sapien velizzle, phat volutpizzle, check it out quis, gravida vizzle, fo shizzle. Pellentesque brizzle tortor. Sed erizzle. Stuff break it down dolor its fo rizzle black tempus pimpin'. Mauris pellentesque nibh bizzle turpis. Pot izzle tortizzle. We gonna chung ma nizzle rhoncizzle da bomb. In hizzle cool boom shackalack dictumst. Donec dapibus. Curabitur tellus yo, pretium eu, izzle izzle, eleifend vitae, nunc. Dope suscipit. Integizzle semper velizzle my shizz purus.".getBytes());
		fos.close();
		Assert.assertTrue(upload.exists());
		
		// load request
    Request request = requestService.read(session, new Request(35));

		// get first action
		if (request.getActions().size() == 0) {
			Assert.fail("Request has no actions; RequestAction required to delete RequestResource");
		}
		RequestAction action = request.getActions().get(0);
		int numberOfDocs = action.getDocuments().size();
		
		RequestDocument document = new RequestDocument();
		document.setFilename(upload.getName());
		document.setTitle("My Test File");
		document.setDocumentType("Other");
		
		request = requestService.addActionDocument(session, request, action, document, upload);
		
		Assert.assertEquals("Document was uploaded", numberOfDocs + 1, request.getActions().get(0).getDocuments().size());
		
		// cleanup
		upload.deleteOnExit();
	}
	
	@Test
	public void getDocument() throws ServiceException {
		
		// load request
    Request request = requestService.read(session, new Request(35));

		// get first action
		if (request.getActions().size() == 0) {
			Assert.fail("Request has no actions; RequestAction required to delete RequestResource");
		}
		RequestAction action = request.getActions().get(0);

		if (action.getDocuments().size() == 0) {
			Assert.fail("RequestAction has no documents to GET");
		}
		RequestDocument document = action.getDocuments().get(0);

		BinaryResponse response = requestDocumentService.getDocument(session, request, action, document);
		
		Assert.assertEquals("Document size is as expected", document.getSize(), response.getResult().length);
	}
	
	@Test
	public void deleteDocument() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));

		// get first action
		if (request.getActions().size() == 0) {
			Assert.fail("Request has no actions; RequestAction required to delete RequestResource");
		}
		RequestAction action = request.getActions().get(0);

		if (action.getDocuments().size() == 0) {
			Assert.fail("RequestAction has no documents to GET");
		}
		RequestDocument document = action.getDocuments().get(0);
		
		Request deleted = requestService.deleteActionDocument(session, request, action, document);
		
		Assert.assertFalse("Deleted RequestDocument in RequestAction documents", Iterables.contains(deleted.getActions().get(0).getDocuments(), document));
	}
	
	@Test
	public void setActionAllocationDates() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has at least one action", request.getActions().size() > 0);
		RequestAction action = request.getActions().get(0);
		
		RequestAllocationDate allocationDate = new RequestAllocationDate();
		allocationDate.setBeginDate(DateTime.now().withTimeAtStartOfDay().withZoneRetainFields(DateTimeZone.UTC));
		allocationDate.setEndDate(allocationDate.getBeginDate().plusYears(1));
		allocationDate.setAllocationDateType("Submitted");
		
		requestService.setActionAllocationDates(session, request, action, allocationDate);
		
		// TODO Assert
	}
	
	@Test
	public void updateActionAllocationDates() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));
		
		// request has actions
		RequestAction action = Iterables.getFirst(request.getActions(), null);
		Assert.assertNotNull("Request has an action", action);
		
		// and action has allocation dates
		RequestAllocationDate allocationDate = Iterables.getFirst(action.getAllocationDates(), null);
		Assert.assertNotNull("Action has an allocation date", allocationDate);
		
		// update allocationDate beginDate and endDate to current time
		// NOTE: this field only records the DATE portion in the database, therefore the value that gets parsed out of
		// API responses is going to be yyyy-MM-dd with time at midnight and timezone at UTC/Zulu time. We explicitly
		// set that here so we can test equality below
		allocationDate.setBeginDate(DateTime.now().withTimeAtStartOfDay().withZoneRetainFields(DateTimeZone.UTC));
		allocationDate.setEndDate(allocationDate.getBeginDate());
		request = requestService.updateActionAllocationDates(session, request, action, allocationDate);
		
		// verify allocationDate was updated
		Assert.assertEquals("Allocation Date was updated", allocationDate, request.getActions().get(0).getAllocationDates().get(0));
	}
	
	@Test
	public void deleteActionAllocationDate() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));
		
		// request has actions
		RequestAction action = Iterables.getFirst(request.getActions(), null);
		Assert.assertNotNull("Request has an action", action);
		
		// and action has allocation dates
		RequestAllocationDate allocationDate = Iterables.getFirst(action.getAllocationDates(), null);
		Assert.assertNotNull("Action has an allocation date", allocationDate);
		
		request = requestService.deleteActionAllocationDates(session, request, action, allocationDate);

		// verify allocationDate was deleted
		Assert.assertFalse("Allocation Date was deleted from Request Action", request.getActions().get(0).getAllocationDates().contains(allocationDate));
	}
	
	@Test
	public void setOpportunityAttribute() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has at least one action", request.getActions().size() > 0);
		RequestAction action = request.getActions().get(0);

		Opportunity opportunity = new Opportunity();
		opportunity.setId(request.getOpportunityId());
		opportunityService.read(session, opportunity);
		
		List<OpportunityAttributeValue> addedValues = new ArrayList<OpportunityAttributeValue>();
		for (AttributeSet<OpportunityAttribute> attrSet : opportunity.getAttributeSets()) {
			for (OpportunityAttribute attr : attrSet.getAttributes()) {
				OpportunityAttributeValue value = new OpportunityAttributeValue();
				value.setId(attr.getId());
				value.setAttributeValue("THIS IS A RESPONSE TO: " + attr.getAttributeName());
				addedValues.add(value);
				requestService.setActionOpportunityAttribute(session, request, action, value);
			}
		}
		
		request = requestService.read(session, request);
		action = request.getActions().get(0);
		for (OpportunityAttributeValue added : addedValues) {
			Assert.assertTrue("OpportunityAttributeValue was added", action.getOpportunityAttributes().contains(added));
		}
	}
	
	@Test
	public void setResourceAttribute() throws ServiceException {
		// load request
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has at least one action", request.getActions().size() > 0);
		RequestAction action = request.getActions().get(0);

		Opportunity opportunity = new Opportunity();
		opportunity.setId(request.getOpportunityId());
		opportunityService.read(session, opportunity);
		
		Assert.assertTrue("Opportunity has resources available", opportunity.getResources().size() > 0);
		
		OpportunityResource oRes = Iterables.find(opportunity.getResources(), new Predicate<OpportunityResource> () {

			@Override
			public boolean apply(OpportunityResource input) {
				try {
					Resource resource = new Resource(input.getId());
					resourceService.read(session, resource);
					return resource.getAttributeSets().size() > 0;
				} catch (ServiceException e) {
					e.printStackTrace();
				}
				return false;
			}
			
		});
		
		Resource resource = new Resource(oRes.getId());
		resourceService.read(session, resource);

		List<ResourceAttributeValue> addedValues = new ArrayList<ResourceAttributeValue>();
		for (AttributeSet<ResourceAttribute> attrSet : resource.getAttributeSets()) {
			for (ResourceAttribute attr : attrSet.getAttributes()) {
				ResourceAttributeValue value = new ResourceAttributeValue();
				value.setId(attr.getId());
				value.setAttributeValue("THIS IS A RESPONSE TO: " + attr.getAttributeName() + " (" + new DateTime() + ")");
				addedValues.add(value);
				requestService.setActionResourceAttribute(session, request, action, value);
			}
		}
		
		request = requestService.read(session, request);
		action = request.getActions().get(0);
		for (ResourceAttributeValue added : addedValues) {
			Assert.assertTrue("ResourceAttributeValue was added", action.getResourceAttributes().contains(added));
		}
	}
	
	@Test
	public void addFieldOfScience() throws ServiceException {
    Request request = requestService.read(session, new Request(35));
		
		RequestFos fos = new RequestFos();
		fos.setPrimary(true);
		fos.setId(31); // {"fosName":"Computer and Information Science and Engineering","fosAbbr":"CIE","fosNum":"300","fosTypeId":31,"isActive":true} 
		
		requestService.addFos(session, request, fos);
		
		request = requestService.read(session, request);
		List<RequestFos> fields = request.getFos();
		
		Assert.assertTrue("Field of Science was added to request", fields.contains(fos));
	}
	
	@Test
	public void deleteFieldOfScience() throws ServiceException {
    Request request = requestService.read(session, new Request(35));

		// Request has FoS to delete
		Assert.assertTrue("Request has FoS to delete", ! request.getFos().isEmpty());
		
		// delete last FoS in the list
		RequestFos fos = Iterables.getLast(request.getFos());
		request = requestService.deleteFos(session, request, fos);
		
		// assert FoS was deleted
		Assert.assertFalse("Field of Science was removed from request", request.getFos().contains(fos));
	}
  
  @Test
  public void addGrant() throws ServiceException {
    Request request = requestService.read(session, new Request(1162847));
    
    RequestGrant grant = new RequestGrant();
    
    grant.setTitle("Testing the Validity of Methods Implemented for the XRAS Submit API");
    grant.setGrantNumber("TEST" + System.currentTimeMillis());
    grant.setFundingAgencyId(1); // NSF
    grant.setProgramOfficerName("Test Officer");
    grant.setProgramOfficerEmail("test.officer@example.com");
    grant.setPiName("Principal Investigator");
    grant.setBeginDate(new DateTime("2014-01-01"));
    grant.setEndDate(new DateTime("2014-12-31"));
    grant.setAwardedAmount(new BigDecimal(100000));
    grant.setAwardedUnits("Dollars");
    grant.setPercentageAward(new BigDecimal(50));
    grant.setPrimaryFosTypeId(31); // CISE
    
    request = requestService.addGrant(session, request, grant);
    
    // Verify
    Assert.assertTrue("Grant was added", request.getGrants().contains(grant));
  }
	
	@Test
	public void updateGrant() throws ServiceException {
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has grants to update", request.getGrants().size() > 0);
		
		// get last grant in list for updating
		RequestGrant grant = Iterables.getLast(request.getGrants());
		
		// update grant number to current millis
		grant.setGrantNumber("TEST" + System.currentTimeMillis());
		
		// do update
		request = requestService.updateGrant(session, request, grant);
		
		Assert.assertEquals("Grant object updated", grant, Iterables.getLast(request.getGrants()));
	}
	
	@Test
	public void deleteGrant() throws ServiceException {
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has grants to delete", request.getGrants().size() > 0);
		
		// get last grant in list for deletion
		RequestGrant grant = request.getGrants().get(request.getGrants().size() - 1);
		
		// delete it
		request = requestService.deleteGrant(session, request, grant);
		
		// assert grant was deleted
		Assert.assertTrue("Grant deleted from request", ! request.getGrants().contains(grant));
	}
	
	@Test
	public void addPublication() throws ServiceException, JsonProcessingException, IOException {
    Request request = requestService.read(session, new Request(35));
		
		RequestPublication publication = new RequestPublication();
		Publication pub = RequestPublication.getObjectMapper().reader(Publication.class).readValue(getClass().getResourceAsStream("/json/publication.json"));
		publication.setPublication(pub);
		
		request = requestService.addPublication(session, request, publication);
		
		// Verify
		Assert.assertTrue("Publication was added", request.getPublications().contains(publication));
	}
	
	@Test
	public void deletePublication() throws ServiceException {
    Request request = requestService.read(session, new Request(35));
		
		Assert.assertTrue("Request has publications to delete", request.getPublications().size() > 0);
		
		// get last pub in list for deletion
		RequestPublication pub = request.getPublications().get(request.getPublications().size() - 1);
		
		// delete it
		request = requestService.deletePublication(session, request, pub);
		
		// assert pub was deleted
		Assert.assertTrue("Publication " + pub.getId() + " deleted from request", ! request.getPublications().contains(pub));
	}
	
}
