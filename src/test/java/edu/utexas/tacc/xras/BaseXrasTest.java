/**
 * 
 */
package edu.utexas.tacc.xras;

import org.apache.log4j.Logger;
import org.junit.After;
import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import edu.utexas.tacc.xras.http.session.XrasSession;

/**
 * @author mrhanlon
 *
 */
@ContextConfiguration("/xras-context.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public abstract class BaseXrasTest {

	protected XrasSession session;
	
	@Before
	public void before() {
		session = new XrasSession("mrhanlon");
	}
	
	@After
	public void after() {
		session = null;
	}

	private Logger logger;
	
	protected Logger getLogger() {
		if (logger == null) {
			logger = Logger.getLogger(this.getClass());
		}
		return logger;
	}
}
