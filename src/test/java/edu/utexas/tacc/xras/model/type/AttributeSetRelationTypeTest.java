/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class AttributeSetRelationTypeTest {

	@Test
	public void validateUrl() {
		AttributeSetRelationType type = new AttributeSetRelationType();
		Assert.assertEquals("BaseUrl is correct", AttributeSetRelationType.BASE_URL + AttributeSetRelationType.TYPE_URL, type.getBaseUrl());
	}

}
