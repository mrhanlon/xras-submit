/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.AllocationDateType;

/**
 * @author mrhanlon
 *
 */
public class AllocationDateTypeTest {

	@Test
	public void validateUrl() {
		AllocationDateType fa = new AllocationDateType();
		Assert.assertEquals("BaseUrl is correct", AllocationDateType.BASE_URL + AllocationDateType.TYPE_URL, fa.getBaseUrl());
	}

}
