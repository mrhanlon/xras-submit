/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class RequestTest {

	@Test
	public void validateUrl() {
		Request model = new Request();
		Assert.assertEquals("BaseUrl is correct", Request.BASE_URL, model.getBaseUrl());
	}

}
