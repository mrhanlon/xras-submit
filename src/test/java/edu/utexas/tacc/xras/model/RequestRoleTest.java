/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.joda.time.DateTime;
import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class RequestRoleTest {

  @Test
  public void toJSON() {
    RequestRole role = new RequestRole();
    role.setRole("PI");
    role.setBeginDate(DateTime.parse("2014-06-15"));
    Assert.assertEquals("correct json output", "{\"beginDate\":\"2014-06-15\",\"roleType\":\"PI\",\"isAccountToBeCreated\":false}", role.toJSON().toString());
  }
  
  @Test
  public void testEqualsForNewRoles() {
    RequestRole a = new RequestRole();
    a.setRole("PI");
    
    RequestRole b = new RequestRole();
    b.setRole("CoPI");
    
    Assert.assertFalse("Equality based on role if isNew()", a.equals(b));
    
    a.setRole("CoPI");
    Assert.assertTrue("Equality based on role if isNew()", a.equals(b));
  }
  
  @Test
  public void testEqualsForExistingRoles() {
    RequestRole a = new RequestRole();
    a.setId(101);
    a.setRole("PI");
    
    RequestRole b = new RequestRole();
    b.setId(101);
    b.setRole("CoPI");
    
    RequestRole c = new RequestRole();
    c.setId(102);
    c.setRole("CoPI");
    
    Assert.assertTrue("Equality based on id if not isNew()", a.equals(b));
    
    Assert.assertFalse("Equality based on id if not isNew()", b.equals(c));
  }
  
  
}
