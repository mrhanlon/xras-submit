/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class OpportunityTest {
	
	@Test
	public void validateUrl() {
		Opportunity model = new Opportunity();
		Assert.assertEquals("BaseUrl is correct", Opportunity.BASE_URL, model.getBaseUrl());
	}

}
