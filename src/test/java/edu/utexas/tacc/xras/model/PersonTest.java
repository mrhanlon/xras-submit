/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class PersonTest {

	@Test
	public void validateUrl() {
		Person model = new Person();
		Assert.assertEquals("BaseUrl is correct", Person.BASE_URL, model.getBaseUrl());
	}

}
