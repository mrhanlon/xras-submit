/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.OpportunityStateType;

/**
 * @author mrhanlon
 *
 */
public class OpportunityStateTypeTest {

	@Test
	public void validateUrl() {
		OpportunityStateType type = new OpportunityStateType();
		Assert.assertEquals("BaseUrl is correct", OpportunityStateType.BASE_URL + type.getTypeUrl(), type.getBaseUrl());
	}

}
