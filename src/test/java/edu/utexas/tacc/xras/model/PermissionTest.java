/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class PermissionTest {
	
	@Test
	public void validateUrl() {
		FundingAgency fa = new FundingAgency();
		Assert.assertEquals("BaseUrl is correct", FundingAgency.BASE_URL, fa.getBaseUrl());
	}

}
