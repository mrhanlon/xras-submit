/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.RequestStatusType;

/**
 * @author mrhanlon
 *
 */
public class RequestStatusTypeTest {

	@Test
	public void validateUrl() {
		RequestStatusType fa = new RequestStatusType();
		Assert.assertEquals("BaseUrl is correct", RequestStatusType.BASE_URL + RequestStatusType.TYPE_URL, fa.getBaseUrl());
	}

}
