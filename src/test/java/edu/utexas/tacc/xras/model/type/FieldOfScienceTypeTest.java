/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import edu.utexas.tacc.xras.BaseXrasTest;
import edu.utexas.tacc.xras.model.type.FieldOfScienceType;
import edu.utexas.tacc.xras.service.type.FieldOfScienceTypeService;

/**
 * @author mrhanlon
 *
 */
public class FieldOfScienceTypeTest extends BaseXrasTest {

	
	@Autowired
	private FieldOfScienceTypeService fosTypeService;
	
	@Test
	public void validateUrl() {
		FieldOfScienceType fa = new FieldOfScienceType();
		Assert.assertEquals("BaseUrl is correct", FieldOfScienceType.BASE_URL + FieldOfScienceType.TYPE_URL, fa.getBaseUrl());
	}
	
}
