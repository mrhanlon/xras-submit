/**
 * 
 */
package edu.utexas.tacc.xras.model;

import junit.framework.Assert;

import org.junit.Test;

/**
 * @author mrhanlon
 *
 */
public class FundingAgencyTest {

	@Test
	public void validateUrl() {
		FundingAgency model = new FundingAgency();
		Assert.assertEquals("BaseUrl is correct", FundingAgency.BASE_URL, model.getBaseUrl());
	}
}
