/**
 * 
 */
package edu.utexas.tacc.xras.model.parser;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import junit.framework.Assert;

import org.junit.Test;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.model.Resource;
import edu.utexas.tacc.xras.model.parser.XrasParser;

/**
 * @author mrhanlon
 *
 */
public class XrasParserTest {
	
	public ApiResponse mockResponse(String source) throws JsonProcessingException, IOException {
		InputStream in = getClass().getResourceAsStream(source);
		JsonNode parsed = new ObjectMapper().readTree(in);
		in.close();
		ApiResponse response = new ApiResponse(200);
		response.setMessage(parsed.get("message").asText());
		response.setResult(parsed.get("result"));
		return response;
	}

	/**
	 * Tests the parsing of JSON model response.
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@Test
	public void testModelParse() throws JsonProcessingException, IOException {
		ApiResponse response = mockResponse("/json/model.json");
		Resource resource = XrasParser.parseModel(response.getResult(), Resource.class);
		Assert.assertEquals("TACC Dell PowerEdge C8220 Cluster with Intel Xeon Phi coprocessors (Stampede)", resource.getResourceName());
	}

	/**
	 * Tests parsing JSON list response.
	 * @throws JsonProcessingException
	 * @throws IOException
	 */
	@Test
	public void testListParse() throws JsonProcessingException, IOException {
		ApiResponse response = mockResponse("/json/model-list.json");
		List<Resource> resources = XrasParser.parseList(response.getResult(), Resource.class);
		
		Assert.assertEquals(119, resources.size());
		Assert.assertEquals("TACC Dell PowerEdge C8220 Cluster with Intel Xeon Phi coprocessors (Stampede)", resources.get(98).getResourceName());
	}
}
