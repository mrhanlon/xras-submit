/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.ResourceNumberType;

/**
 * @author mrhanlon
 *
 */
public class ResourceNumberTypeTest {

	@Test
	public void validateUrl() {
		ResourceNumberType type = new ResourceNumberType();
		Assert.assertEquals("BaseUrl is correct", ResourceNumberType.BASE_URL + type.getTypeUrl(), type.getBaseUrl());
	}

}
