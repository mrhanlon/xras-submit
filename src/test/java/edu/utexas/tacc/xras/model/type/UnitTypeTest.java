/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.UnitType;

/**
 * @author mrhanlon
 *
 */
public class UnitTypeTest {

	@Test
	public void validateUrl() {
		UnitType fa = new UnitType();
		Assert.assertEquals("BaseUrl is correct", UnitType.BASE_URL + UnitType.TYPE_URL, fa.getBaseUrl());
	}

}
