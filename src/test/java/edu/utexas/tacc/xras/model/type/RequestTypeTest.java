/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import junit.framework.Assert;

import org.junit.Test;

import edu.utexas.tacc.xras.model.type.RequestType;

/**
 * @author mrhanlon
 *
 */
public class RequestTypeTest {

	@Test
	public void validateUrl() {
		RequestType fa = new RequestType();
		Assert.assertEquals("BaseUrl is correct", RequestType.BASE_URL + RequestType.TYPE_URL, fa.getBaseUrl());
	}

}
