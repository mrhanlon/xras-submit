/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author mrhanlon
 *
 */
public class RequestPublication extends BaseXrasModel<RequestPublication> {

	private Publication publication;
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getId()
	 */
	@Override
	@JsonProperty("publicationId")
	public int getId() {
		return id;
	}

	/**
	 * @return the publication
	 */
	public Publication getPublication() {
		return publication;
	}

	/**
	 * @param publication the publication to set
	 */
	public void setPublication(Publication publication) {
		this.publication = publication;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/publications";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		return getId();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestPublication other = (RequestPublication) obj;
		if (id != other.id) {
		  return false;
		}
		if (publication == null) {
			if (other.publication != null) {
				return false;
			}
		} else if (!publication.equals(other.publication)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#toJSON()
	 */
	@Override
	public ObjectNode toJSON() {
		return (ObjectNode) getObjectMapper().valueToTree(publication);
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#toJSON(java.lang.Class[])
	 */
	@Override
	public ObjectNode toJSON(Class<?>... mixins) {
		ObjectMapper mapper = getObjectMapper();
		for (Class<?> mixin : mixins) {
			mapper.addMixInAnnotations(getClass(), mixin);
		}
		return mapper.valueToTree(publication);
	}

}
