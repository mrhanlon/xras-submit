/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;


/**
 * @author mrhanlon
 *
 */
@JsonInclude(Include.NON_NULL)
public abstract class BaseXrasModel<T> extends BaseModel implements XrasModel<T> {

	protected int id;
	
	protected BaseXrasModel() {}
	
	protected BaseXrasModel(int id) {
		this.id = id;
	}

	@Override
	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String getInstanceUrl() {
		return this.getBaseUrl() + "/" + this.getId();
	}
	
	/*
	 * Force implementation of java.lang.Object#equals(java.lang.Object)
	 * (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public abstract boolean equals(Object object);
	
	/*
	 * Force implementation of java.lang.Object#hashCode()
	 * (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public abstract int hashCode();

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.model.XrasModel#isNew()
	 */
	@Override
	public boolean isNew() {
		return id == 0;
	}

	@Override
	public int compareTo(XrasModel<T> o) {
		return this.getId() - o.getId();
	}
}
