/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class UnitType extends Type {

	public UnitType() {
	}

	public UnitType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/units";

	private String unitType;

	private String displayUnitType;

	@Override
	@JsonProperty("unitTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the unitType
	 */
	public String getUnitType() {
		return unitType;
	}

	/**
	 * @param unitType
	 *            the unitType to set
	 */
	public void setUnitType(String unitType) {
		this.unitType = unitType;
	}

	/**
	 * @return the displayUnitType
	 */
	public String getDisplayUnitType() {
		return displayUnitType;
	}

	/**
	 * @param displayUnitType
	 *            the displayUnitType to set
	 */
	public void setDisplayUnitType(String displayUnitType) {
		this.displayUnitType = displayUnitType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
