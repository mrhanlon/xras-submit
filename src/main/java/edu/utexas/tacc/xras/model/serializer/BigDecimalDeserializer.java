/**
 * 
 */
package edu.utexas.tacc.xras.model.serializer;

import java.io.IOException;
import java.math.BigDecimal;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.JsonToken;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;

/**
 * Parses out invalid BigDecimal characters such as ",".
 * @author mrhanlon
 *
 */
public class BigDecimalDeserializer extends JsonDeserializer<BigDecimal> {

  /* (non-Javadoc)
   * @see com.fasterxml.jackson.databind.JsonDeserializer#deserialize(com.fasterxml.jackson.core.JsonParser, com.fasterxml.jackson.databind.DeserializationContext)
   */
  @Override
  public BigDecimal deserialize(JsonParser jp, DeserializationContext ctxt) throws IOException, JsonProcessingException {
    JsonToken t = jp.getCurrentToken();
    if (t == JsonToken.VALUE_NUMBER_INT || t == JsonToken.VALUE_NUMBER_FLOAT) {
        return jp.getDecimalValue();
    }
    // String is ok too, can easily convert
    if (t == JsonToken.VALUE_STRING) { // let's do implicit re-parse
        String text = jp.getText().trim();
        if (text.length() == 0) {
            return null;
        }
        String decimal = text.replaceAll("[^\\d.]", "");
        String sign = text.startsWith("-") ? "-" : "";
        try {
            return new BigDecimal(sign + decimal);
        } catch (IllegalArgumentException iae) {
            throw ctxt.weirdStringException(text, BigDecimal.class, "not a valid representation");
        }
    }
    // Otherwise, no can do:
    throw ctxt.mappingException(BigDecimal.class, t);
  }

}
