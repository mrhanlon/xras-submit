/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

import edu.utexas.tacc.xras.model.serializer.BigDecimalDeserializer;

/**
 * @author mrhanlon
 *
 */
public class RequestResource extends BaseXrasModel<RequestResource> {

	private String resourceName;
	
	private BigDecimal amount;
	
	private String comments;
	
	private String type;

	/**
	 * @return the id
	 */
	@JsonProperty("resourceId")
	public int getId() {
		return id;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the amount
	 */
	@JsonSerialize(using=ToStringSerializer.class)
  @JsonDeserialize(using=BigDecimalDeserializer.class)
	public BigDecimal getAmount() {
		return amount;
	}

	/**
	 * @param amount the amount to set
	 */
	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	/**
	 * @return the comments
	 */
	public String getComments() {
		return comments;
	}

	/**
	 * @param comments the comments to set
	 */
	public void setComments(String comments) {
		this.comments = comments;
	}

	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/resources";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((amount == null) ? 0 : amount.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		
		RequestResource other = (RequestResource) obj;
		if (id != other.id)
			return false;
		if (amount == null) {
			if (other.amount != null)
				return false;
		} else if (!amount.equals(other.amount))
			return false;
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestResource [resourceId=" + id + ", amount=" + amount + "]";
	}
	
}
