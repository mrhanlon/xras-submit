/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class RequestStatusType extends Type {

	public RequestStatusType() {
	}

	public RequestStatusType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/request_status";

	private String requestStatusType;

	private String displayRequestStatusType;

	@Override
	@JsonProperty("requestStatusTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the requestStatusType
	 */
	public String getRequestStatusType() {
		return requestStatusType;
	}

	/**
	 * @param requestStatusType
	 *            the requestStatusType to set
	 */
	public void setRequestStatusType(String requestStatusType) {
		this.requestStatusType = requestStatusType;
	}

	/**
	 * @return the displayRequestStatusType
	 */
	public String getDisplayRequestStatusType() {
		return displayRequestStatusType;
	}

	/**
	 * @param displayRequestStatusType
	 *            the displayRequestStatusType to set
	 */
	public void setDisplayRequestStatusType(String displayRequestStatusType) {
		this.displayRequestStatusType = displayRequestStatusType;
	}

	@Override
	public String getTypeUrl() {
		// TODO Auto-generated method stub
		return TYPE_URL;
	}

}
