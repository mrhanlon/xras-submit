/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class OpportunityRule {
  
  private int opportunityId;

  private boolean canSubmitNewRequest = false;
  
  private List<String> noAvailableActionsReason;
  
  private List<OpportunityAvailableAction> availableActionRequests;

  /**
   * @return the opportunityId
   */
  public int getOpportunityId() {
    return opportunityId;
  }

  /**
   * @param opportunityId the opportunityId to set
   */
  public void setOpportunityId(int opportunityId) {
    this.opportunityId = opportunityId;
  }

  /**
   * @return the canSubmitNewRequest
   */
  public boolean isCanSubmitNewRequest() {
    return canSubmitNewRequest;
  }

  /**
   * @param canSubmitNewRequest the canSubmitNewRequest to set
   */
  public void setCanSubmitNewRequest(boolean canSubmitNewRequest) {
    this.canSubmitNewRequest = canSubmitNewRequest;
  }

  /**
   * @return the noAvailableActionsReason
   */
  public List<String> getNoAvailableActionsReason() {
    return noAvailableActionsReason;
  }

  /**
   * @param noAvailableActionsReason the noAvailableActionsReason to set
   */
  public void setNoAvailableActionsReason(List<String> noAvailableActionsReason) {
    this.noAvailableActionsReason = noAvailableActionsReason;
  }

  /**
   * @return the availableActionRequests
   */
  public List<OpportunityAvailableAction> getAvailableActionRequests() {
    return availableActionRequests;
  }

  /**
   * @param availableActionRequests the availableActionRequests to set
   */
  public void setAvailableActionRequests(List<OpportunityAvailableAction> availableActionRequests) {
    this.availableActionRequests = availableActionRequests;
  }
  
  
}
