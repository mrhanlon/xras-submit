/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class OpportunityAttribute {

	private int id;
	
	private String attributeName;

	/**
	 * @return the id
	 */
	@JsonProperty("opportunityAttributeId")
	public int getId() {
		return id;
	}
  
  public int getAttributeId() {
    return id;
  }

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the attributeName
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * @param attributeName the attributeName to set
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}
}
