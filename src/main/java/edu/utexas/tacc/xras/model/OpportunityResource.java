/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class OpportunityResource {

	private int id;
	
	private String resourceName;
	
	private String displayResourceName;
	
	private String resourceState;
	
	private String siteProviderComments;
	
	private List<OpportunityResourceNumber> numbers = new ArrayList<OpportunityResourceNumber>();
	
	private List<ResourceDependency> requiredResources = new ArrayList<ResourceDependency>();

	/**
	 * @return the id
	 */
	@JsonProperty("resourceId")
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the displayResourceName
	 */
	public String getDisplayResourceName() {
		return displayResourceName;
	}

	/**
	 * @param displayResourceName the displayResourceName to set
	 */
	public void setDisplayResourceName(String displayResourceName) {
		this.displayResourceName = displayResourceName;
	}

	/**
	 * @return the resourceState
	 */
	public String getResourceState() {
		return resourceState;
	}

	/**
	 * @param resourceState the resourceState to set
	 */
	public void setResourceState(String resourceState) {
		this.resourceState = resourceState;
	}

	/**
	 * @return the siteProviderComments
	 */
	public String getSiteProviderComments() {
		return siteProviderComments;
	}

	/**
	 * @param siteProviderComments the siteProviderComments to set
	 */
	public void setSiteProviderComments(String spComments) {
		this.siteProviderComments = spComments;
	}

	/**
	 * @return the numbers
	 */
	public List<OpportunityResourceNumber> getNumbers() {
		return numbers;
	}

	/**
	 * @param numbers the numbers to set
	 */
	public void setNumbers(List<OpportunityResourceNumber> numbers) {
		this.numbers = numbers;
	}

	/**
	 * @return the requiredResources
	 */
	public List<ResourceDependency> getRequiredResources() {
		return requiredResources;
	}

	/**
	 * @param requiredResources the requiredResources to set
	 */
	public void setRequiredResources(List<ResourceDependency> requiredResources) {
		this.requiredResources = requiredResources;
	}
}
