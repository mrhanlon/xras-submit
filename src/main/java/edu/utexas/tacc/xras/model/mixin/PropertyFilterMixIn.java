/**
 * 
 */
package edu.utexas.tacc.xras.model.mixin;

import com.fasterxml.jackson.annotation.JsonFilter;

/**
 * @author mrhanlon
 *
 */
@JsonFilter("propertyFilter")
public class PropertyFilterMixIn {

}
