/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class Resource extends BaseXrasModel<Resource> {

	public Resource() {}
	
	public Resource(int id) {
		super(id);
	}
	
	private String resourceName;
	private String displayResourceName;
	private String resourceType;
	private String description;
	private String units;
	private String organization;
	private List<AttributeSet<ResourceAttribute>> attributeSets;

	public static final String BASE_URL = "/resources";

	@Override
	public String getBaseUrl() {
		// TODO Auto-generated method stub
		return BASE_URL;
	}

	@Override
	@JsonProperty("resourceId")
	public int getId() {
		return id;
	}

	/**
	 * @return the resourceName
	 */
	public String getResourceName() {
		return resourceName;
	}

	/**
	 * @param resourceName the resourceName to set
	 */
	public void setResourceName(String resourceName) {
		this.resourceName = resourceName;
	}

	/**
	 * @return the displayResourceName
	 */
	public String getDisplayResourceName() {
		return displayResourceName;
	}

	/**
	 * @param displayResourceName the displayResourceName to set
	 */
	public void setDisplayResourceName(String displayResourceName) {
		this.displayResourceName = displayResourceName;
	}

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the units
	 */
	public String getUnits() {
		return units;
	}

	/**
	 * @param units the units to set
	 */
	public void setUnits(String units) {
		this.units = units;
	}

	/**
	 * @return the organization
	 */
	public String getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(String organization) {
		this.organization = organization;
	}

	/**
	 * @return the attributeSets
	 */
	public List<AttributeSet<ResourceAttribute>> getAttributeSets() {
		return attributeSets;
	}

	/**
	 * @param attributeSets the attributeSets to set
	 */
	public void setAttributeSets(List<AttributeSet<ResourceAttribute>> attributeSets) {
		this.attributeSets = attributeSets;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((organization == null) ? 0 : organization.hashCode());
		result = prime * result + ((resourceName == null) ? 0 : resourceName.hashCode());
		result = prime * result + ((resourceType == null) ? 0 : resourceType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resource other = (Resource) obj;
		if (organization == null) {
			if (other.organization != null)
				return false;
		} else if (!organization.equals(other.organization))
			return false;
		if (resourceName == null) {
			if (other.resourceName != null)
				return false;
		} else if (!resourceName.equals(other.resourceName))
			return false;
		if (resourceType == null) {
			if (other.resourceType != null)
				return false;
		} else if (!resourceType.equals(other.resourceType))
			return false;
		return true;
	}

}
