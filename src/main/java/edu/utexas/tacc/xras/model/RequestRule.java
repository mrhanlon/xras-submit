/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

/**
 * @author mrhanlon
 *
 */
public class RequestRule {

  private int requestId;
  
  private List<String> allowedOperations;
  
  private List<String> allowedActions;
  
  private List<RequestActionRule> existingActions;

  /**
   * @return the requestId
   */
  public int getRequestId() {
    return requestId;
  }

  /**
   * @param requestId the requestId to set
   */
  public void setRequestId(int requestId) {
    this.requestId = requestId;
  }

  /**
   * @return the allowedOperations
   */
  public List<String> getAllowedOperations() {
    return allowedOperations;
  }

  /**
   * @param allowedOperations the allowedOperations to set
   */
  public void setAllowedOperations(List<String> allowedOperations) {
    this.allowedOperations = allowedOperations;
  }

  /**
   * @return the allowedActions
   */
  public List<String> getAllowedActions() {
    return allowedActions;
  }

  /**
   * @param allowedActions the allowedActions to set
   */
  public void setAllowedActions(List<String> allowedActions) {
    this.allowedActions = allowedActions;
  }

  /**
   * @return the existingActions
   */
  public List<RequestActionRule> getExistingActions() {
    return existingActions;
  }

  /**
   * @param existingActions the existingActions to set
   */
  public void setExistingActions(List<RequestActionRule> existingActions) {
    this.existingActions = existingActions;
  }
}
