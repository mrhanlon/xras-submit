/**
 * 
 */
package edu.utexas.tacc.xras.model;

/**
 * @author mrhanlon
 *
 */
public class PublicationData extends BaseModel {

	private int id;
	
	private PublicationField field;
	
	private String value;

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the field
	 */
	public PublicationField getField() {
		return field;
	}

	/**
	 * @param field the field to set
	 */
	public void setField(PublicationField field) {
		this.field = field;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
}
