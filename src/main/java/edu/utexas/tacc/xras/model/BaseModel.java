/**
 * 
 */
package edu.utexas.tacc.xras.model;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.fasterxml.jackson.datatype.joda.JodaModule;

/**
 * @author mrhanlon
 *
 */
public abstract class BaseModel {
  
  public static ObjectMapper getObjectMapper() {
    ObjectMapper mapper = new ObjectMapper();
    mapper.registerModule(new JodaModule());
    return mapper;
  }
  
  public ObjectNode toJSON() {
    return (ObjectNode) getObjectMapper().valueToTree(this);
  }

  public ObjectNode toJSON(Class<?>... mixins) {
    ObjectMapper mapper = getObjectMapper();
    for (Class<?> mixin : mixins) {
      mapper.addMixInAnnotations(getClass(), mixin);
    }
    return mapper.valueToTree(this);
  }

  /**
   * Prevent the deserialization from failing when new properties are encountered. Log with warning.
   * @param key
   * @param value
   */
  @JsonAnySetter
  public void handleUnknown(String key, Object value) {
    Logger.getLogger(getClass()).warn("Unknown field while deserializing JSON: {\"" + key + "\": \"" + value + "\"}");
  }
}
