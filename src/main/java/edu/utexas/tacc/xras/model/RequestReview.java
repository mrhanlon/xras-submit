/**
 * 
 */
package edu.utexas.tacc.xras.model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class RequestReview extends BaseXrasModel<RequestReview> {

	private List<ActionReview> actionReviews;
	
	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getId()
	 */
	@JsonProperty("requestId")
	@Override
	public int getId() {
		return id;
	}

	/**
	 * @return the actionReviews
	 */
	@JsonProperty("actions")
	public List<ActionReview> getActionReviews() {
		return actionReviews;
	}

	/**
	 * @param actionReviews the actionReviews to set
	 */
	public void setActionReviews(List<ActionReview> actionReviews) {
		this.actionReviews = actionReviews;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/requests/" + getId() + "/reviews";
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#getInstanceUrl()
	 */
	@Override
	public String getInstanceUrl() {
		return getBaseUrl();
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((actionReviews == null) ? 0 : actionReviews.hashCode());
		result = prime * result + id;
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestReview other = (RequestReview) obj;
		if (actionReviews == null) {
			if (other.actionReviews != null) {
				return false;
			}
		} else if (!actionReviews.equals(other.actionReviews)) {
			return false;
		}
		if (id != other.id) {
			return false;
		}
		return true;
	}

	
}
