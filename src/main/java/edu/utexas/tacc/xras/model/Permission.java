/**
 *
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties({"id"})
public class Permission extends BaseXrasModel<Permission> {

	public Permission() {}
	
	public static final String BASE_URL = "/permissions";
	
	private String permissionType;
	
	private String displayPermissionType;

	@Override
	@JsonProperty("permissionTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the permissionType
	 */
	public String getPermissionType() {
		return permissionType;
	}

	/**
	 * @param permissionType the permissionType to set
	 */
	public void setPermissionType(String permissionType) {
		this.permissionType = permissionType;
	}

	/**
	 * @return the displayPermissionType
	 */
	public String getDisplayPermissionType() {
		return displayPermissionType;
	}

	/**
	 * @param displayPermissionType the displayPermissionType to set
	 */
	public void setDisplayPermissionType(String displayPermissionType) {
		this.displayPermissionType = displayPermissionType;
	}

	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((permissionType == null) ? 0 : permissionType.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Permission other = (Permission) obj;
		if (permissionType == null) {
			if (other.permissionType != null)
				return false;
		} else if (!permissionType.equals(other.permissionType))
			return false;
		return true;
	}

}
