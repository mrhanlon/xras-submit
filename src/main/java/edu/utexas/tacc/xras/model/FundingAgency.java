/**
 *
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mrhanlon
 *
 */
public class FundingAgency extends BaseXrasModel<FundingAgency> {

	public FundingAgency() {}
	
	public FundingAgency(int id) {
		super(id);
	}

	public static final String BASE_URL = "/funding_agencies";

	private String fundingAgencyName;

	private String fundingAgencyAbbr;

	@Override
	@JsonProperty("fundingAgencyId")
	public int getId() {
		return id;
	}

	/**
	 * @return the fundingAgencyName
	 */
	public String getFundingAgencyName() {
		return fundingAgencyName;
	}

	/**
	 * @param fundingAgencyName the fundingAgencyName to set
	 */
	public void setFundingAgencyName(String fundingAgencyName) {
		this.fundingAgencyName = fundingAgencyName;
	}

	/**
	 * @return the fundingAgencyAbbr
	 */
	public String getFundingAgencyAbbr() {
		return fundingAgencyAbbr;
	}

	/**
	 * @param fundingAgencyAbbr the fundingAgencyAbbr to set
	 */
	public void setFundingAgencyAbbr(String fundingAgencyAbbr) {
		this.fundingAgencyAbbr = fundingAgencyAbbr;
	}

	@Override
	public String getBaseUrl() {
		return BASE_URL;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((fundingAgencyAbbr == null) ? 0 : fundingAgencyAbbr.hashCode());
		result = prime * result + ((fundingAgencyName == null) ? 0 : fundingAgencyName.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FundingAgency other = (FundingAgency) obj;
		if (fundingAgencyAbbr == null) {
			if (other.fundingAgencyAbbr != null)
				return false;
		} else if (!fundingAgencyAbbr.equals(other.fundingAgencyAbbr))
			return false;
		if (fundingAgencyName == null) {
			if (other.fundingAgencyName != null)
				return false;
		} else if (!fundingAgencyName.equals(other.fundingAgencyName))
			return false;
		return true;
	}

}
