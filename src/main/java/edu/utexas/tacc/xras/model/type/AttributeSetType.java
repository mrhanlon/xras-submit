/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 * 
 */
public class AttributeSetType extends Type {

	public AttributeSetType() {
	}

	public AttributeSetType(int id) {
		super(id);
	}

	public static final String TYPE_URL = "/attribute_sets";

	private String attributeSetType;

	private String displayAttributeSetType;

	@Override
	@JsonProperty("attributeSetTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the attributeSetType
	 */
	public String getAttributeSetType() {
		return attributeSetType;
	}

	/**
	 * @param attributeSetType
	 *            the attributeSetType to set
	 */
	public void setAttributeSetType(String attributeSetType) {
		this.attributeSetType = attributeSetType;
	}

	/**
	 * @return the displayAttributeSetType
	 */
	public String getDisplayAttributeSetType() {
		return displayAttributeSetType;
	}

	/**
	 * @param displayAttributeSetType
	 *            the displayAttributeSetType to set
	 */
	public void setDisplayAttributeSetType(String displayAttributeSetType) {
		this.displayAttributeSetType = displayAttributeSetType;
	}

	@Override
	public String getTypeUrl() {
		return TYPE_URL;
	}

}
