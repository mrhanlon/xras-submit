/**
 *
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;


/**
 * @author mrhanlon
 *
 */
@JsonIgnoreProperties({"typeUrl", "baseUrl"})
public abstract class Type extends BaseXrasModel<Type> {

	protected Type() {}
	
	protected Type(int id) {
		super(id);
	}

	public static final String BASE_URL = "/types";
	
	public abstract String getTypeUrl();

	@Override
	public String getBaseUrl() {
		return BASE_URL + getTypeUrl();
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Type other = (Type) obj;
		
		// Types are "read only" so it should be sufficient to just compare ID values
		return getId() == other.getId();
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.BaseXrasModel#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * getId();
		return result;

	}

}
