/**
 *
 */
package edu.utexas.tacc.xras.model.type;

import com.fasterxml.jackson.annotation.JsonProperty;

import edu.utexas.tacc.xras.model.Type;

/**
 * @author mrhanlon
 *
 */
public class ResourceType extends Type {

	public ResourceType() {}

	public ResourceType(int id) {
		super(id);
	}

	private String resourceType;

	private String displayResourceType;

	private String description;

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.model.XrasModel#getId()
	 */
	@Override
	@JsonProperty("resourceTypeId")
	public int getId() {
		return id;
	}

	/**
	 * @return the resourceType
	 */
	public String getResourceType() {
		return resourceType;
	}

	/**
	 * @param resourceType the resourceType to set
	 */
	public void setResourceType(String resourceType) {
		this.resourceType = resourceType;
	}

	/**
	 * @return the displayResourceType
	 */
	public String getDisplayResourceType() {
		return displayResourceType;
	}

	/**
	 * @param displayResourceType the displayResourceType to set
	 */
	public void setDisplayResourceType(String displayResourceType) {
		this.displayResourceType = displayResourceType;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.model.Type#getTypeUrl()
	 */
	@Override
	public String getTypeUrl() {
		return "/resources";
	}

}
