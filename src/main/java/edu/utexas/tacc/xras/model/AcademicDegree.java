/**
 * 
 */
package edu.utexas.tacc.xras.model;

/**
 * @author mrhanlon
 *
 */
public class AcademicDegree extends BaseModel {

	public AcademicDegree() {}
	
	private String degree;
	
	private String field;
	
	/**
	 * @return the degree
	 */
	public String getDegree() {
		return degree;
	}
	
	/**
	 * @param degree the degree to set
	 */
	public void setDegree(String degree) {
		this.degree = degree;
	}
	
	/**
	 * @return the field
	 */
	public String getField() {
		return field;
	}
	
	/**
	 * @param field the field to set
	 */
	public void setField(String field) {
		this.field = field;
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		// TODO Auto-generated method stub
		return super.equals(obj);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		// TODO Auto-generated method stub
		return super.hashCode();
	}
}
