/**
 * 
 */
package edu.utexas.tacc.xras.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.node.ObjectNode;

/**
 * @author mrhanlon
 *
 */
public interface XrasModel<T> extends Comparable<XrasModel<T>> {

	public int getId();

	public void setId(int id);

	@JsonIgnore
	public String getBaseUrl();

	@JsonIgnore
	public String getInstanceUrl();

	@JsonIgnore
	public boolean isNew();
	
	public ObjectNode toJSON();
	
	public ObjectNode toJSON(Class<?>...mixins);
}
