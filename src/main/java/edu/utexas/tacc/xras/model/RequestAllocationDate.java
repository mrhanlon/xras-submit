/**
 * 
 */
package edu.utexas.tacc.xras.model;

import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

import edu.utexas.tacc.xras.model.serializer.JodaDateOnlySerializer;

/**
 * @author mrhanlon
 *
 */
public class RequestAllocationDate extends BaseXrasModel<RequestAllocationDate> {
	
	private String allocationDateType;
	
	private DateTime beginDate;
	
	private DateTime endDate;

	/**
	 * @return the id
	 */
	@JsonProperty("allocationDateId")
	public int getId() {
		return id;
	}

	/**
	 * @return the allocationDateType
	 */
	public String getAllocationDateType() {
		return allocationDateType;
	}

	/**
	 * @param allocationDateType the allocationDateType to set
	 */
	public void setAllocationDateType(String allocationDateType) {
		this.allocationDateType = allocationDateType;
	}

	/**
	 * @return the beginDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(DateTime beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	@JsonSerialize(using = JodaDateOnlySerializer.class)
	public DateTime getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(DateTime endDate) {
		this.endDate = endDate;
	}

	/* (non-Javadoc)
	 * @see edu.utexas.tacc.xras.model.XrasModel#getBaseUrl()
	 */
	@Override
	public String getBaseUrl() {
		return "/allocation_dates";
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((allocationDateType == null) ? 0 : allocationDateType.hashCode());
		result = prime * result + ((beginDate == null) ? 0 : beginDate.hashCode());
		result = prime * result + ((endDate == null) ? 0 : endDate.hashCode());
		return result;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		RequestAllocationDate other = (RequestAllocationDate) obj;
		
		if (allocationDateType == null) {
			if (other.allocationDateType != null) {
				return false;
			}
		} else if (!allocationDateType.equals(other.allocationDateType)) {
			return false;
		}
		if (beginDate == null) {
			if (other.beginDate != null) {
				return false;
			}
		} else if (!beginDate.equals(other.beginDate)) {
			return false;
		}
		if (endDate == null) {
			if (other.endDate != null) {
				return false;
			}
		} else if (!endDate.equals(other.endDate)) {
			return false;
		}
		return true;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RequestAllocationDate [allocationDateType=" + allocationDateType + ", beginDate=" + beginDate + ", endDate=" + endDate + "]";
	}

}
