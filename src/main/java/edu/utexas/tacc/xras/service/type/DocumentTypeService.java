/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.DocumentType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class DocumentTypeService extends TypeService<DocumentType> {
	
	public DocumentTypeService() {
		super(DocumentType.class);
	}

}
