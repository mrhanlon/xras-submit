/**
 * 
 */
package edu.utexas.tacc.xras.service;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.Opportunity;

/**
 * @author mrhanlon
 *
 */
@Service
public class OpportunityService extends BasicXrasService<Opportunity> {
	
	public OpportunityService() {
		super(Opportunity.class);
	}

}
