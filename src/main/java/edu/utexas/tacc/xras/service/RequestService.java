/**
 * 
 */
package edu.utexas.tacc.xras.service;

import java.io.File;

import org.apache.commons.lang.StringUtils;
import org.apache.http.HttpEntity;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.node.ObjectNode;

import edu.utexas.tacc.xras.ServiceException;
import edu.utexas.tacc.xras.http.ApiResponse;
import edu.utexas.tacc.xras.http.session.XrasSession;
import edu.utexas.tacc.xras.model.BaseXrasModel;
import edu.utexas.tacc.xras.model.Opportunity;
import edu.utexas.tacc.xras.model.OpportunityAttributeValue;
import edu.utexas.tacc.xras.model.Person;
import edu.utexas.tacc.xras.model.Request;
import edu.utexas.tacc.xras.model.RequestAction;
import edu.utexas.tacc.xras.model.RequestAllocationDate;
import edu.utexas.tacc.xras.model.RequestDocument;
import edu.utexas.tacc.xras.model.RequestFos;
import edu.utexas.tacc.xras.model.RequestGrant;
import edu.utexas.tacc.xras.model.RequestPublication;
import edu.utexas.tacc.xras.model.RequestResource;
import edu.utexas.tacc.xras.model.RequestRole;
import edu.utexas.tacc.xras.model.RequestValidation;
import edu.utexas.tacc.xras.model.ResourceAttributeValue;
import edu.utexas.tacc.xras.model.parser.XrasParser;
import edu.utexas.tacc.xras.model.type.RequestType;

/**
 * @author mrhanlon
 * 
 */
@Service
public class RequestService extends BasicXrasService<Request> {
	
	private static final Logger logger = Logger.getLogger(RequestService.class);
	
	public RequestService() {
		super(Request.class);
	}
	
	/**
	 * <code>POST /v1/requests</code>
	 * @param session
	 * @param request
	 * @param opportunity
	 * @param requestType
	 * @throws ServiceException 
	 */
	public Request newRequest(XrasSession session, Request request, Opportunity opportunity, RequestType requestType) throws ServiceException {
		ObjectNode body = BaseXrasModel.getObjectMapper().createObjectNode();
		body.put("opportunityId", opportunity.getId());
		body.put("requestType", requestType.getRequestType());
		if (StringUtils.isNotBlank(request.getRequestNumber())) {
			body.put("requestNumber", request.getRequestNumber());
		}
		try {
			ApiResponse response = getClient().post(request.getBaseUrl(), body.toString(), session);
			if (response.isOk()) {
				return XrasParser.parseModel(response.getResult(), Request.class);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
	
	public Request delete(XrasSession session, Request request) throws ServiceException {
		try {
			ApiResponse response = getClient().delete(request.getInstanceUrl(), session);
			if (response.isOk()) {
				return XrasParser.parseModel(response.getResult(), Request.class);
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (Exception e) {
			logger.error("Error deleting request id: " + request.getId(), e);
			throw new ServiceException("Error deleting request id: " + request.getId(), e);
		}
	}

	/**
	 * <code>POST /v1/requests/&lt;requestId&gt;/roles/&lt;roleType&gt;/&lt;username&gt;</code>
	 * @param session
	 * @param request
	 * @param role
	 * @return
	 */
	public Request addRole(XrasSession session, Request request, RequestRole role, Person person) throws ServiceException {
		String url = request.getInstanceUrl() + role.getInstanceUrl() + "/" + person.getUsername();
		ObjectNode body = role.toJSON();
		body.setAll(person.toJSON().retain("firstName", "middleName", "lastName", "email", "phone", "organization"));
		return postChildObject(session, request, url, body.toString());
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/roles/&lt;roleId&gt;</code>
	 * @param session
	 * @param request
	 * @param role
	 * @return
	 */
	public Request updateRole(XrasSession session, Request request, RequestRole role) throws ServiceException {
		String url = request.getInstanceUrl() + role.getInstanceUrl();
		return putChildObject(session, request, url, role.toJSON().toString());
	}

	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/roles/&lt;roleId&gt;</code>
	 * @param session
	 * @param request
	 * @param role
	 * @return
	 */
	public Request deleteRole(XrasSession session, Request request, RequestRole role) throws ServiceException {
		String url = request.getInstanceUrl() + role.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}


	/**
	 * <code>POST /v1/requests/&lt;requestId&gt;/actions</code>
	 * @return
	 */
	public Request addAction(XrasSession session, Request request, RequestAction action) throws ServiceException {
		// RequestAction objects are hung off the Request object
		String url = request.getInstanceUrl() + action.getInstanceUrl();
		return postChildObject(session, request, url, action.toJSON().toString());
	}


	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;</code>
	 * @return
	 */
	public Request updateAction(XrasSession session, Request request, RequestAction action) throws ServiceException {

		String url = request.getInstanceUrl() + action.getInstanceUrl();
		return putChildObject(session, request, url, action.toJSON().toString());
	}


	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;</code>
	 * @return
	 */
	public Request deleteAction(XrasSession session, Request request, RequestAction action) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}

  /**
   * <code>POST /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/submit</code>
   * @param session
   * @param request
   * @param action
   * @return
   * @throws ServiceException 
   */
  public Request submitAction(XrasSession session, Request request, RequestAction action) throws ServiceException {
    String url = request.getInstanceUrl() + action.getInstanceUrl() + "/submit";
    return postChildObject(session, request, url, "");
  }

  /**
   * <code>POST /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/validate</code>
   * @param session
   * @param request
   * @param action
   * @return
   * @throws ServiceException 
   */
  public RequestValidation validateAction(XrasSession session, Request request, RequestAction action) throws ServiceException {
    String url = request.getInstanceUrl() + action.getInstanceUrl() + "/validate";
    try {

      ApiResponse response = getClient().get(url, session);
      if (response.isOk()) {
        RequestValidation validation = XrasParser.parseModel(response.getResult(), RequestValidation.class);
        return validation;
      } else {
        throw new ServiceException(response.getMessage(), response.getStatusCode());
      }
    } catch (Exception e) {
      logger.error(e);
      throw new ServiceException(e);
    }
  }

  /**
   * <code>POST /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/allocation_dates</code>
   * @return
   */
  public Request addAllocationDate(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
    String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getBaseUrl();
    return postChildObject(session, request, url, date.toJSON().toString());
  }

  /**
   * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/allocation_dates/&lt;allocationDateId&gt;</code>
   * @return
   */
  public Request updateAllocationDate(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
    String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getInstanceUrl();
    return putChildObject(session, request, url, date.toJSON().toString());
  }

  /**
   * <code>DELETE /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/allocation_dates/&lt;allocationDateId&gt;</code>
   * @return
   */
  public Request deleteAllocationDate(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
    String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getInstanceUrl();
    return deleteChildObject(session, request, url);
  }

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/resources/&lt;resourceId&gt;</code>
	 * @return
	 */
	public Request addUpdateActionResource(XrasSession session, Request request, RequestAction action, RequestResource resource) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + resource.getInstanceUrl();
		return putChildObject(session, request, url, resource.toJSON().toString());
	}


	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/resources/&lt;resourceId&gt;</code>
	 * @return
	 */
	public Request deleteActionResource(XrasSession session, Request request, RequestAction action, RequestResource resource) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + resource.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}


	/**
	 * <code>POST /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/documents</code>
	 * @return
	 */
	public Request addActionDocument(XrasSession session, Request request, RequestAction action, RequestDocument document, File upload) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + document.getBaseUrl();
		try {
			// build multipart request body
			HttpEntity reqEntity = MultipartEntityBuilder.create()
				.addTextBody("title", document.getTitle())
				.addTextBody("filename", document.getFilename())
				.addTextBody("documentType", document.getDocumentType())
				.addBinaryBody("document", upload, ContentType.APPLICATION_OCTET_STREAM, document.getFilename())
				.build();
			
			ApiResponse response = getClient().post(url, reqEntity, session);
			if (response.isOk()) {
				Request created = XrasParser.parseModel(response.getResult(), Request.class);
				return created;
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}			
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}


	/**
	 * <code>GET /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/documents/&lt;documentId&gt;</code>
	 * @return
	 */
	public Request deleteActionDocument(XrasSession session, Request request, RequestAction action, RequestDocument document) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + document.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}


	/**
	 * <code>POST /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/alloc_dates</code>
	 * @return
	 */
	public Request setActionAllocationDates(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getBaseUrl();
		return postChildObject(session, request, url, date.toJSON().toString());
	}


	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/alloc_dates/&lt;allocDateId&gt;</code>
	 * @return
	 */
	public Request updateActionAllocationDates(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getInstanceUrl();
		return putChildObject(session, request, url, date.toJSON().toString());
	}


	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/alloc_dates/&lt;allocDateId&gt;</code>
	 * @return
	 */
	public Request deleteActionAllocationDates(XrasSession session, Request request, RequestAction action, RequestAllocationDate date) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + date.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/opportunity_attributes/&lt;opportunityAttributeId&gt;</code>
	 * @return
	 */
	public Request setActionOpportunityAttribute(XrasSession session, Request request, RequestAction action, OpportunityAttributeValue attributeValue) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + attributeValue.getInstanceUrl();
		return putChildObject(session, request, url, attributeValue.toJSON().toString());
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/actions/&lt;actionId&gt;/resource_attributes/&lt;resourceAttributeId&gt;</code>
	 * @return
	 */
	public Request setActionResourceAttribute(XrasSession session, Request request, RequestAction action, ResourceAttributeValue attributeValue) throws ServiceException {
		String url = request.getInstanceUrl() + action.getInstanceUrl() + attributeValue.getInstanceUrl();
		return putChildObject(session, request, url, attributeValue.toJSON().toString());
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/fos/&lt;fosTypeId&gt;</code>
	 * @return
	 */
	public Request addFos(XrasSession session, Request request, RequestFos fos) throws ServiceException {
		String url = request.getInstanceUrl() + fos.getInstanceUrl();
		return putChildObject(session, request, url, fos.toJSON().toString());
	}

	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/fos/&lt;fosTypeId&gt;</code>
	 * @return
	 */
	public Request deleteFos(XrasSession session, Request request, RequestFos fos) throws ServiceException {
		String url = request.getInstanceUrl() + fos.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}

	/**
	 * <code>POST /v1/requests/&lt;requestId&gt;/grants</code>
	 * @return
	 */
	public Request addGrant(XrasSession session, Request request, RequestGrant grant) throws ServiceException {
		String url = request.getInstanceUrl() + grant.getBaseUrl();
		return postChildObject(session, request, url, grant.toJSON().toString());
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/grants/&lt;grantId&gt;</code>
	 * @return
	 */
	public Request updateGrant(XrasSession session, Request request, RequestGrant grant) throws ServiceException {
		String url = request.getInstanceUrl() + grant.getInstanceUrl();
		return putChildObject(session, request, url, grant.toJSON().toString());

	}

	/**
	 * <code>DELETE /v1/requests/&lt;requestId&gt;/grants/&lt;grantId&gt;</code>
	 * @return
	 */
	public Request deleteGrant(XrasSession session, Request request, RequestGrant grant) throws ServiceException {
		String url = request.getInstanceUrl() + grant.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}
	
	public Request addPublication(XrasSession session, Request request, RequestPublication publication) throws ServiceException {
		String url = request.getInstanceUrl() + publication.getBaseUrl();
		return postChildObject(session, request, url, publication.toJSON().toString());
	}
	
	public Request deletePublication(XrasSession session, Request request, RequestPublication publication) throws ServiceException {
		String url = request.getInstanceUrl() + publication.getInstanceUrl();
		return deleteChildObject(session, request, url);
	}

//	/**
//	 * <code>POST /v1/requests/&lt;requestId&gt;/conflicts</code>
//	 * @return
//	 */
//	public Request addConflict(XrasSession session, Request request, RequestConflict conflict) throws ServiceException {
//		String url = request.getInstanceUrl() + conflict.getBaseUrl();
//		return postChildObject(session, request, url, conflict.toJSON().toString());
//	}
//
//	/**
//	 * <code>PUT /v1/requests/&lt;requestId&gt;/conflicts/&lt;conflictId&gt;</code>
//	 * @return
//	 */
//	public Request updateConflict(XrasSession session, Request request, RequestConflict conflict) throws ServiceException {
//		String url = request.getInstanceUrl() + conflict.getInstanceUrl();
//		return postChildObject(session, request, url, conflict.toJSON().toString());
//	}
//
//	/**
//	 * <code>DELETE /v1/requests/&lt;requestId&gt;/conflicts/&lt;conflictId&gt;</code>
//	 * @return
//	 */
//	public Request deleteConflict(XrasSession session, Request request, RequestConflict conflict) throws ServiceException {
//		String url = request.getInstanceUrl() + conflict.getInstanceUrl();
//		return deleteChildObject(session, request, url);
//	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/attributes</code>
	 * @return
	 */
	public Request addUpdateAttributes(XrasSession session, Request request) throws ServiceException {
		return addUpdateAttributes(session, request, "keywords", "abstract", "title", "requestNumber", "requestStatus");
	}

	/**
	 * <code>PUT /v1/requests/&lt;requestId&gt;/attributes</code>
	 * @return
	 */
	public Request addUpdateAttributes(XrasSession session, Request request, String...properties) throws ServiceException {
		try {
			ApiResponse response = getClient().put(request.getInstanceUrl() + "/attributes", request.toJSON().retain(properties).toString(), session);
			if (response.getStatusCode() == 200) {
				Request saved = XrasParser.parseModel(response.getResult(), Request.class);
				return saved;
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
  
  protected Request postChildObject(XrasSession session, Request request, String objectUrl, String body) throws ServiceException {
    try {
      
      ApiResponse response = getClient().post(objectUrl, body, session);
      if (response.isOk()) {
        Request created = XrasParser.parseModel(response.getResult(), Request.class);
        return created;
      } else {
        throw new ServiceException(response.getMessage(), response.getStatusCode());
      }     
    } catch (Exception e) {
      logger.error(e);
      throw new ServiceException(e);
    }
  }
	
	protected Request putChildObject(XrasSession session, Request request, String objectUrl, String body) throws ServiceException {
		try {
			
			ApiResponse response = getClient().put(objectUrl, body, session);
			if (response.isOk()) {
				Request created = XrasParser.parseModel(response.getResult(), Request.class);
				return created;
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}			
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}

	protected Request deleteChildObject(XrasSession session, Request request, String objectUrl) throws ServiceException {
		try {
			
			ApiResponse response = getClient().delete(objectUrl, session);
			if (response.isOk()) {
				Request created = XrasParser.parseModel(response.getResult(), Request.class);
				return created;
			} else {
				throw new ServiceException(response.getMessage(), response.getStatusCode());
			}			
		} catch (Exception e) {
			logger.error(e);
			throw new ServiceException(e);
		}
	}
}
