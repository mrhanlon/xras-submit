/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.UnitType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class UnitTypeService extends TypeService<UnitType> {

	/**
	 * @param type
	 */
	public UnitTypeService() {
		super(UnitType.class);
	}

}
