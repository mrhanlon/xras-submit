/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.RoleType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class RoleTypeService extends TypeService<RoleType> {

	public RoleTypeService() {
		super(RoleType.class);
	}
}
