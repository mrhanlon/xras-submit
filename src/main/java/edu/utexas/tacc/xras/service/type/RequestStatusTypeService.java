/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.RequestStatusType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class RequestStatusTypeService extends TypeService<RequestStatusType> {
	
	public RequestStatusTypeService() {
		super(RequestStatusType.class);
	}

}
