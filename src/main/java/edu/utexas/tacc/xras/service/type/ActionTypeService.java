/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.ActionType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class ActionTypeService extends TypeService<ActionType> {

	public ActionTypeService() {
		super(ActionType.class);
	}
}
