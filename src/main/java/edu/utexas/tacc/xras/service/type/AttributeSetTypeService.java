/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.AttributeSetType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class AttributeSetTypeService extends TypeService<AttributeSetType> {

	public AttributeSetTypeService() {
		super(AttributeSetType.class);
	}
}
