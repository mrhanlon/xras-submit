/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.AllocationDateType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class AllocationDateTypeService extends TypeService<AllocationDateType> {
	
	public AllocationDateTypeService() {
		super(AllocationDateType.class);
	}

}
