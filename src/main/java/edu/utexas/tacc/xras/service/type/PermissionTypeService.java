/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.PermissionType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class PermissionTypeService extends TypeService<PermissionType> {
	
	public PermissionTypeService() {
		super(PermissionType.class);
	}

}
