/**
 * 
 */
package edu.utexas.tacc.xras.service;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.Panel;

/**
 * @author mrhanlon
 *
 */
@Service
public class PanelService extends BasicXrasService<Panel> {
	
	public PanelService() {
		super(Panel.class);
	}
}
