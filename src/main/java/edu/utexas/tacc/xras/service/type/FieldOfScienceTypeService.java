/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.FieldOfScienceType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class FieldOfScienceTypeService extends TypeService<FieldOfScienceType> {

	public FieldOfScienceTypeService() {
		super(FieldOfScienceType.class);
	}

}
