/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.OpportunityStateType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class OpportunityStateTypeService extends TypeService<OpportunityStateType> {

	public OpportunityStateTypeService() {
		super(OpportunityStateType.class);
	}
}
