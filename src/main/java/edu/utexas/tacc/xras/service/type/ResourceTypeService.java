/**
 * 
 */
package edu.utexas.tacc.xras.service.type;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.type.ResourceType;
import edu.utexas.tacc.xras.service.TypeService;

/**
 * @author mrhanlon
 *
 */
@Service
public class ResourceTypeService extends TypeService<ResourceType> {

	/**
	 * @param type
	 */
	public ResourceTypeService() {
		super(ResourceType.class);
	}

}
