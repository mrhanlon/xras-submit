/**
 * 
 */
package edu.utexas.tacc.xras.service;

import org.springframework.stereotype.Service;

import edu.utexas.tacc.xras.model.Resource;

/**
 * @author mrhanlon
 *
 */
@Service
public class ResourceService extends BasicXrasService<Resource> {
	
	public ResourceService() {
		super(Resource.class);
	}

}