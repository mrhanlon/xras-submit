/**
 * 
 */
package edu.utexas.tacc.xras.http;

import java.io.IOException;

import org.apache.http.Header;
import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;

/**
 * @author mrhanlon
 *
 */
public class BinaryResponseHandler implements ResponseHandler<BinaryResponse> {

	private static final Logger logger = Logger.getLogger(BinaryResponseHandler.class);
	
	/* (non-Javadoc)
	 * @see org.apache.http.client.ResponseHandler#handleResponse(org.apache.http.HttpResponse)
	 */
	@Override
	public BinaryResponse handleResponse(HttpResponse httpResponse) throws ClientProtocolException, IOException {
		StatusLine status = httpResponse.getStatusLine();
		
		
		BinaryResponse response = new BinaryResponse(status.getStatusCode());
		
		response.setHeaders(httpResponse.getAllHeaders());
		
		Header[] contentDisposition = httpResponse.getHeaders("Content-Disposition");
		if (contentDisposition.length == 1) {
			response.setContentDisposition(contentDisposition[0].getValue());
		}
		
		Header[] contentType = httpResponse.getHeaders("Content-Type");
		if (contentType.length == 1) {
			response.setContentType(contentType[0].getValue());
		}

		byte[] responseBody = EntityUtils.toByteArray(httpResponse.getEntity());
		response.setResult(responseBody);
		
		if (logger.isDebugEnabled()) {
			logger.debug(responseBody.length);
		}
		
		return response;
	}

}
