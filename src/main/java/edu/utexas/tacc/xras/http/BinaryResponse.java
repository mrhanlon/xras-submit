/**
 * 
 */
package edu.utexas.tacc.xras.http;

import org.apache.http.Header;


/**
 * @author mrhanlon
 *
 */
public class BinaryResponse {
	public BinaryResponse(int statusCode) {
		this(statusCode, null);
	}
	
	public BinaryResponse(int statusCode, String message) {
		this(statusCode, message, null);
	}
	
	public BinaryResponse(int statusCode, String message, byte[] result) {
		this.statusCode = statusCode;
		this.message = message;
		this.result = result;
	}
	
	private int statusCode;
	
	private String message;
	
	private byte[] result;
	
	private Header[] headers;
	
	private String contentDisposition;
	
	private String contentType;
	
	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public byte[] getResult() {
		return result;
	}

	public void setResult(byte[] result) {
		this.result = result;
	}

	/**
	 * @return the headers
	 */
	public Header[] getHeaders() {
		return headers;
	}

	/**
	 * @param headers the headers to set
	 */
	public void setHeaders(Header[] headers) {
		this.headers = headers;
	}

	/**
	 * @return the contentDisposition
	 */
	public String getContentDisposition() {
		return contentDisposition;
	}

	/**
	 * @param contentDisposition the contentDisposition to set
	 */
	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	/**
	 * @return the contentType
	 */
	public String getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
}
