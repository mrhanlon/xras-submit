/**
 * 
 */
package edu.utexas.tacc.xras.http.session;

/**
 * @author mrhanlon
 *
 */
public class XrasSession {

	public XrasSession(String userContext) {
		this.userContext = userContext;
	}
	
	private final String userContext;

	public String getUserContext() {
		return userContext;
	}
	
}
