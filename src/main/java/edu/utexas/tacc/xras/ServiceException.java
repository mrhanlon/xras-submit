/**
 * 
 */
package edu.utexas.tacc.xras;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * @author mrhanlon
 *
 */
public class ServiceException extends Exception implements Iterable<Throwable> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public ServiceException() {
		super();
	}
	
	public ServiceException(Throwable cause) {
		super(cause);
	}
	
	public ServiceException(String message) {
		super(message);
	}

	public ServiceException(String message, Throwable cause) {
		super(message, cause);
	}
	
	public ServiceException(String message, int statusCode) {
		super(message);
		this.statusCode = statusCode;
	}

	public ServiceException(String message, int statusCode, Throwable cause) {
		super(message, cause);
		this.statusCode = statusCode;
	}
	
	private int statusCode;

	/**
	 * @return the statusCode
	 */
	public int getStatusCode() {
		return statusCode;
	}

	/**
	 * @param statusCode the statusCode to set
	 */
	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	/* (non-Javadoc)
	 * @see java.lang.Throwable#toString()
	 */
	@Override
	public String toString() {
		if (statusCode > 0) {
			return "HTTP " + getStatusCode() + ": " + getMessage();
		} else {
			return super.toString();
		}
	}

	@Override
	public Iterator<Throwable> iterator() {
		return new Iterator<Throwable>() {

			ServiceException first = ServiceException.this;
			Throwable cause = first.getCause();

			@Override
			public void remove() {
				throw new UnsupportedOperationException();
			}

			@Override
			public Throwable next() {
				Throwable throwable = null;
				if (first != null) {
					throwable = first;
					first = null;
				} else if (cause != null) {
					throwable = cause;
					cause = cause.getCause();
				} else {
					throw new NoSuchElementException();
				}
				return throwable;
			}

			@Override
			public boolean hasNext() {
				if (first != null || cause != null) {
					return true;
				}
				return false;
			}
		};
	}
}